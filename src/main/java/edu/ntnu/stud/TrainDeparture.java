package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * This is the class that contains info on train departures.
 *
 * @author Carine Margrethe Rondeel
 * @version 1.0
 */

public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private LocalTime delay;

  /**
   * Constructor when all parameters are submitted. Delay automatically set to 00:00
   *
   * @param departureTime departure time of the train (cannot be null, and must be between 00:00
   *                      and 23:59)
   * @param line line name for the train (cannot be null)
   * @param trainNumber train number
   * @param destination destination
   * @param track track
   * @throws IllegalArgumentException if the departure time is null, or outside 00:00 to 23:59
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination,
                        int track) throws IllegalArgumentException {
    if (departureTime == null || departureTime.isBefore(LocalTime.MIN)
        || departureTime.isAfter(LocalTime.MAX)) {
      throw new IllegalArgumentException("Departure time cannot be null, and needs to be between"
          + " 00:00 and 23:59.");
    }

    if (line == null || line.isEmpty()) {
      throw new IllegalArgumentException("Line cannot be null or empty.");
    }

    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number cannot be less than or equal to 0.");
    }

    if (destination == null || destination.isEmpty()) {
      throw new IllegalArgumentException("Destination cannot be null or empty.");
    }

    if (track < -1 || track == 0) {
      throw new IllegalArgumentException("Track cannot be less than -1 or equal to zero.");
    }

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = LocalTime.MIN;
  }

  /**
   * Constructor when all parameters except track are submitted. Delay automatically set to 00:00.
   * Track gets set to -1.
   *
   * @param departureTime departure time
   * @param line line
   * @param trainNumber train number
   * @param destination destination
   * @throws IllegalArgumentException if the departure time is null, or outside 00:00 to 23:59
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination)
      throws IllegalArgumentException {
    this(departureTime, line, trainNumber, destination, -1);
  }

  /**
   * Retrieves the departure time.
   *
   * @return departure time
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Retrieves the line number.
   *
   * @return line
   */
  public String getLine() {
    return line;
  }

  /**
   * Retrieves the train number.
   *
   * @return train number
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Retrieves the destination.
   *
   * @return destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Retrieves the track number.
   *
   * @return track number
   */
  public int getTrack() {
    return track;
  }

  /**
   * Retrieves the delay.
   *
   * @return delay
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Changes the track number to the newTrack parameter. Also checks if the submitted track is less
   * than -1 or equal to zero, and throws an exception if it is.
   *
   * @param newTrack new track number
   * @throws IllegalArgumentException if track is less than -1 or equal to 0
   */
  public void setTrack(int newTrack) throws IllegalArgumentException {
    if (newTrack < -1 || newTrack == 0) {
      throw new IllegalArgumentException("Track cannot be less than -1 or equal to zero.");
    }
    this.track = newTrack;
  }

  /**
   * Changes the delay to the newDelay parameter. Also checks if the submitted delay is null, less
   * than 00:00 or more than 23:59, and throws an exception if it is.
   *
   * @param newDelay new delay
   * @throws IllegalArgumentException if delay is null or outside 00:00 to 23:59
   */
  public void setDelay(LocalTime newDelay) throws IllegalArgumentException {
    if (newDelay == null || newDelay.isBefore(LocalTime.MIN)
        || newDelay.isAfter(LocalTime.MAX)) {
      throw new IllegalArgumentException("Delay cannot be null, and needs to be between"
          + " 00:00 and 23:59.");
    }
    this.delay = newDelay;
  }

  /**
   * Returns a string that contains info regarding the departure. Only prints delay if it's not
   * zero, and track as long as it's not -1.
   *
   * @return formatted string of departure information
   */
  @Override
  public String toString() {
    String checkedDelay;
    String checkedTrack;

    if (delay.equals(LocalTime.MIN)) {
      checkedDelay = "";
    } else {
      checkedDelay = String.valueOf(delay);
    }
    if (track == -1) {
      checkedTrack = "";
    } else {
      checkedTrack = String.valueOf(track);
    }

    return String.format("%-16s%-8s%-16s%-16s%-9s%-10s",
        departureTime, line, trainNumber, destination, checkedDelay, checkedTrack);
  }
}
