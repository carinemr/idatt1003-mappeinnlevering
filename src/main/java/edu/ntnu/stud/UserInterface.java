package edu.ntnu.stud;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * This is the class for the user interface.
 *
 * @author Carine Margrethe Rondeel
 * @version 1.0
 */
public class UserInterface {
  private static Scanner in;
  private static TrainDepartureRegister tdr;

  //Static variables for the main menu
  private static final int PRINT_DEP_BOARD = 1;
  private static final int SEARCH_BY_TRAIN_NR = 2;
  private static final int SEARCH_BY_DESTINATION = 3;
  private static final int ADD_DEPARTURE = 4;
  private static final int REMOVE_DEPARTURE = 5;
  private static final int CHANGE_DELAY = 6;
  private static final int ASSIGN_TRACK = 7;
  private static final int CHANGE_TIME = 8;
  private static final int PRINT_ALL = 9;
  private static final int EXIT_APP = 10;

  /**
   * The constructor is made private to prevent instantiation of objects of UserInterface.
   */
  private UserInterface() {
  }

  /**
   * The class that initializes the user interface. Also prints the departure board, so it's easily
   * visible from the start that there are some initial departures registered.
   */
  public static void init() {
    tdr = new TrainDepartureRegister();
    in = new Scanner(System.in);
    initialDepartures();
    printDepartureBoard();
  }

  /**
   * The class that starts the user interface process. This has the switch case menu that the user
   * interacts with.
   */
  public static void start() {
    boolean exitMenu = false;

    while (!exitMenu) {
      printMenuOptions();
      int option = scanInputInt();
      switch (option) {
        case PRINT_DEP_BOARD -> printDepartureBoard();
        case SEARCH_BY_TRAIN_NR -> searchByTrainNumber();
        case SEARCH_BY_DESTINATION -> searchByDestination();
        case ADD_DEPARTURE -> addDeparture();
        case REMOVE_DEPARTURE -> removeDeparture();
        case CHANGE_DELAY -> changeDelay();
        case ASSIGN_TRACK -> assignTrack();
        case CHANGE_TIME -> changeTime();
        case PRINT_ALL -> printAllDepartures();
        case EXIT_APP -> exitMenu = true;
        default -> System.out.println("Please choose a valid option!\n");
      }
    }
    end();
  }

  /**
   * A method that ends the application, by first printing a message about it, then closing the
   * scanner to release the resources and then exiting with code 0 which means everything went as
   * expected.
   */
  private static void end() {
    System.out.println("Shutting down application...");
    in.close();
    System.exit(0);
  }

  /**
   * Creates some initial departures, so there are some on the departure board from the get go.
   *
   * @throws IllegalArgumentException if one or more of the departures were not created correctly,
   *      and prints the reason
   */
  private static void initialDepartures() throws IllegalArgumentException {
    try {
      tdr.addTrainDepartureToRegister(LocalTime.of(13, 30),
          "L12", 32, "Kongsberg", 1);
      tdr.addTrainDepartureToRegister(LocalTime.of(13, 29),
          "L12", 31, "Eidsvoll", 3);
      tdr.addTrainDepartureToRegister(LocalTime.of(13, 34),
          "L1", 10, "Lillestrøm");
    } catch (IllegalArgumentException e) {
      System.out.println("One or more of the initial departures were not added, for the following "
          + "reason: " + e.getMessage() + "\n");
    }
  }

  /**
   * Formats and prints the departure board, with the current time in the top right corner, and
   * all subtitles and information being formatted to fit.
   */
  private static void printDepartureBoard() {
    StringBuilder departuresString = new StringBuilder();

    departuresString.append(String.format("%40s%30s", "DEPARTURE BOARD", "CURRENT TIME: "
            + tdr.getTime()))
        .append("\n----------------------------------------------------------------------\n");
    departuresString.append(String.format("%-16s%-8s%-16s%-16s%-9s%-10s",
        "DEPARTURE TIME", "LINE", "TRAIN NUMBER", "DESTINATION", "DELAY", "TRACK")).append("\n");
    tdr.getDepartureTable().forEach((TrainDeparture departure) ->
        departuresString.append(departure.toString()).append("\n"));
    System.out.println(departuresString);
  }

  /**
   * Prints what the options for the menu are, including which number should be written to do the
   * mentioned tasks.
   */
  private static void printMenuOptions() {
    System.out.println("------ Please input one of the following options ------");
    System.out.println("1: Print departure board");
    System.out.println("2: Search for departure based on train number");
    System.out.println("3: Find all departures to a certain destination");
    System.out.println("4: Add a new departure");
    System.out.println("5: Remove an existing departure");
    System.out.println("6: Add/change delay for departure");
    System.out.println("7: Assign new track for departure");
    System.out.println("8: Change the current time");
    System.out.println("9: Print all registered departures");
    System.out.println("10: Quit the application");

    System.out.println("\nWrite the number corresponding to what you want to do: ");
  }

  /**
   * Method to search through the register, by the user submitted train number. Checks if it's
   * valid and prints the found departure with fitting titles kind of like the departure table,
   * or throws exception if something goes wrong.
   *
   * @throws IllegalArgumentException if train number is invalid, or nothing registered for it
   */
  private static void searchByTrainNumber() throws IllegalArgumentException {
    try {
      System.out.println("Please write the train number of the departure you want to search for: ");
      int trainNr = scanInputInt();
      String trainInfo = tdr.getDepartureInformationStringTrainNr(trainNr);

      if (trainInfo.isEmpty()) {
        throw new IllegalArgumentException("Train number submitted is either invalid or there is"
            + " no departure registered with it. Please try again from the main menu.");
      }

      System.out.println(String.format("%-16s%-8s%-16s%-16s%-9s%-10s",
          "DEPARTURE TIME", "LINE", "TRAIN NUMBER", "DESTINATION", "DELAY", "TRACK") + "\n"
          + trainInfo + "\n");
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Searches through the register according to the user submitted destination, and prints all
   * departures going to this destination, regardless of if it has a track or not.
   *
   * @throws NoSuchElementException if there are no departures to the given destination
   */
  private static void searchByDestination() throws NoSuchElementException {
    try {
      StringBuilder depDestinationString = new StringBuilder();
      System.out.println("Please write the destination you want to search for:");
      String destination = scanInputString();
      List<TrainDeparture> destDepList = tdr.getDepartureGivenDestination(destination);

      if (destDepList.isEmpty()) {
        throw new NoSuchElementException("There are no departures to the given destination.");
      }

      depDestinationString.append(String.format("%-16s%-8s%-16s%-16s%-9s%-10s",
          "DEPARTURE TIME", "LINE", "TRAIN NUMBER", "DESTINATION", "DELAY", "TRACK")).append("\n");
      destDepList.forEach((TrainDeparture departure) ->
          depDestinationString.append(departure.toString()).append("\n"));

      System.out.println(depDestinationString);
    } catch (NoSuchElementException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Method to add a departure. Takes input for all necessary parameters from the user, and checks
   * if track should be submitted or not. If unexpected answer to that question is given, departure
   * is automatically added without track and a reason to why is printed to user.
   *
   * @throws IllegalArgumentException if certain input to the departure is invalid
   * @throws NullPointerException if certain input to the departure is invalid
   * @throws DateTimeException if hours or minutes submitted is invalid
   */
  private static void addDeparture() throws IllegalArgumentException, NullPointerException,
      DateTimeException {
    try {
      System.out.println("Please fill in the information for the new departure!");
      System.out.print("Firstly, departure time.\nDeparture hour: ");
      int hour = scanInputInt();
      System.out.print("Departure minutes: ");
      int minutes = scanInputInt();
      checkValidTimeInput(hour, minutes);
      LocalTime departureTime = LocalTime.of(hour, minutes);
      System.out.print("Line: ");
      String line = scanInputString();
      System.out.print("Train number: ");
      int trainNr = scanInputInt();
      System.out.print("Destination: ");
      String destination = scanInputString();
      System.out.println("Do you want to submit the track? (Y/n) ");
      String answerIfTrack = scanInputString();
      if (answerIfTrack.equalsIgnoreCase("y")) {
        System.out.print("Track: ");
        int track = scanInputInt();
        tdr.addTrainDepartureToRegister(departureTime, line, trainNr, destination, track);
        System.out.println("Departure with track was added!\n");
      } else if (answerIfTrack.equalsIgnoreCase("n")) {
        tdr.addTrainDepartureToRegister(departureTime, line, trainNr, destination);
        System.out.println("Departure without track was added!\n");
      } else {
        tdr.addTrainDepartureToRegister(departureTime, line, trainNr, destination);
        System.out.println("The departure was automatically added without the track, due to an "
            + "invalid answer regarding if you wanted to add a track or not. Since all other "
            + "values were valid it was added regardless. If this was a mistake, please use"
            + " the remove option, or you can add the track by choosing 6 on the menu.\n");
      }
    } catch (IllegalArgumentException | NullPointerException | DateTimeException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Removes the departure from the train number submitted by the user. Also asks if the user is
   * sure they want to delete, in case of unexpected answer or a no, nothing is done, and they are
   * returned to main menu.
   *
   * @throws IllegalArgumentException if train number is invalid
   * @throws NoSuchElementException if there are no departures registered with this train number
   */
  private static void removeDeparture() throws IllegalArgumentException, NoSuchElementException {
    try {
      System.out.println("Please write the train number of the departure you want to remove: ");
      int trainNr = scanInputInt();
      System.out.println("Are you sure you want to remove this departure? (Y/n) ");
      String answerIfRemove = scanInputString();
      if (answerIfRemove.equalsIgnoreCase("y")) {
        tdr.removeDepartureGivenTrainNr(trainNr);
        System.out.println("Train number " + trainNr + " was successfully removed.\n");
      } else if (answerIfRemove.equalsIgnoreCase("n")) {
        System.out.println("Understood, the departure was not removed!\n");
      } else {
        System.out.println("Your answer was invalid, and therefore nothing was done. If you want"
            + " to try to delete the departure again, you can do so by choosing it from the main"
            + " menu again.\n");
      }
    } catch (IllegalArgumentException | NoSuchElementException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Changes the delay for the departure given by user, and also takes in the hours and minutes
   * delayed separately. Either adds the delay or throws exception.
   *
   * @throws IllegalArgumentException if the train number is zero or less than zero, or if new
   *      delay is attempted.to be set earlier than current time
   * @throws NullPointerException if the delay is null or if no departures are found
   * @throws DateTimeException if hours or minutes submitted is invalid
   */
  private static void changeDelay() throws NullPointerException, IllegalArgumentException,
      DateTimeException {
    try {
      System.out.println("Please write the train number of the departure you want to change the "
          + "delay for: ");
      int trainNr = scanInputInt();

      System.out.println("How many hours delayed is train number " + trainNr + "? ");
      int hour = scanInputInt();

      System.out.println("And how many minutes delayed is train number " + trainNr + "? ");
      int minutes = scanInputInt();

      checkValidTimeInput(hour, minutes);

      tdr.changeDelayGivenTrainNr(trainNr, LocalTime.of(hour, minutes));
      System.out.println("The delay was added/changed!\n");
    } catch (NullPointerException | IllegalArgumentException | DateTimeException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Assigns a new track or reassigns a track to a deoarture. Takes input for the train number and
   * the new track, and either changes the track or throws exception.
   *
   * @throws IllegalArgumentException if the train number is zero or less than zero
   * @throws NullPointerException if there are no departures found for the given train number
   */
  private static void assignTrack() throws IllegalArgumentException, NullPointerException {
    try {
      System.out.println("Please write the train number of the departure you want to change the "
          + "track for: ");
      int trainNr = scanInputInt();
      System.out.println("Write what the new track should be: ");
      int track = scanInputInt();

      tdr.assignTrackGivenTrainNr(trainNr, track);
      System.out.println("The track has been updated!\n");
    } catch (IllegalArgumentException | NullPointerException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Method to change the current time. Takes input for what the new current time should be and
   * changes it if its valid, or throws exception.
   *
   * @throws IllegalArgumentException if the new time is before current time
   * @throws DateTimeException if hours or minutes submitted is invalid
   */
  private static void changeTime() throws IllegalArgumentException, DateTimeException {
    try {
      System.out.println("The current time should be on the format hh:mm, and goes by the 24-hour "
          + "clock.");
      System.out.println("Write what the new current hour should be:");
      int hour = scanInputInt();
      System.out.println("Write what the new current minutes should be:");
      int minutes = scanInputInt();

      checkValidTimeInput(hour, minutes);

      tdr.setTime(LocalTime.of(hour, minutes));
      System.out.println("The current time has been updated!\n");
    } catch (IllegalArgumentException | DateTimeException e) {
      System.out.println(e.getMessage() + "\n");
    }
    waitToReturnToMenu();
  }

  /**
   * Prints all departures currently in the register, regardless of if they have a track or not.
   */
  private static void printAllDepartures() {
    String allDepStr = tdr.toString();

    if (allDepStr.isEmpty()) {
      System.out.println("There are no departures registered/left to print.\n");
    } else {
      String departuresString = String.format("%35s%35s", "ALL DEPARTURES", "CURRENT TIME: "
          + tdr.getTime())
          + "\n----------------------------------------------------------------------\n"
          + String.format("%-16s%-8s%-16s%-16s%-9s%-10s",
          "DEPARTURE TIME", "LINE", "TRAIN NUMBER", "DESTINATION", "DELAY", "TRACK")
          + "\n" + allDepStr;
      System.out.println(departuresString);
    }
    waitToReturnToMenu();
  }

  /**
   * Scans the submitted input to make sure it's an integer. Does the nextLine after nextInt() to
   * absorb when the user presses enter, since this is not absorbed when taking in an int.
   *
   * @return integer input from user
   */
  private static int scanInputInt() {
    while (!in.hasNextInt()) {
      System.out.println("Your input is not allowed, please input an integer and try "
          + "again: ");
      in.next();
    }
    int input = in.nextInt();
    in.nextLine();
    return input;
  }

  /**
   * Checks that the input from user cannot be an empty string.
   *
   * @return string input from user
   */
  private static String scanInputString() {
    while (!in.hasNextLine()) {
      System.out.println("Your input is not allowed, please input a string that is not empty.");
      in.next();
    }
    return in.nextLine();
  }

  /**
   * Checks that the amount of hours and minutes are valid, so that the program stops and throws
   * in case it's outside 00:00 to 23:59, as this could lead to problems with LocalTime if not
   * handled properly.
   *
   * @param hour amount of hours
   * @param minutes amount of minutes
   * @throws DateTimeException if the amount for either hours or minutes are out of range
   */
  private static void checkValidTimeInput(int hour, int minutes) throws DateTimeException {
    if ((hour < 0 || hour > 23) || (minutes < 0 || minutes > 59)) {
      throw new DateTimeException("The amount of hours have to be between 0-23 and amount of "
          + "minutes have to be between 0-59, please try again with valid values!");
    }
  }

  /**
   * Method that makes user have to submit 'Ok' before returning to the main menu. This is so it's
   * easier for the user to see what happened to their previous inquiry before trying something
   * else. In case the user doesn't type OK properly for three attempts, the method automatically
   * sends them back to the menu, so they don't get stuck here forever.
   */
  private static void waitToReturnToMenu() {
    int autoReturn = 0;
    while (autoReturn < 3) {
      System.out.println("Type OK to return to main menu: ");
      String returnMain = scanInputString();
      if (returnMain.equalsIgnoreCase("ok")) {
        System.out.println("Returning to main menu...\n");
        start();
      } else {
        System.out.println("Input not accepted!\n");
        autoReturn++;
      }
    }
    System.out.println("Automatically returning to main menu after too many failed attempts...\n");
    start();
  }
}