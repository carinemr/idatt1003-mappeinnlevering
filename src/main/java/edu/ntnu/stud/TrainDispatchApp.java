package edu.ntnu.stud;

/**
 * This is the main class to run the train dispatch application.
 *
 * @author Carine Margrethe Rondeel
 * @version 1.0
 */

public class TrainDispatchApp {

  /**
   * The class that calls on init() and start() from UserInterface to initialize and start the
   * program.
   *
   * @param args arguments for the main method
   */
  public static void main(String[] args) {
    UserInterface.init();
    UserInterface.start();
  }
}
