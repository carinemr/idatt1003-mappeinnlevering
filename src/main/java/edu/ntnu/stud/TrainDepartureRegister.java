package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * This class contains the register that contains all added train departures.
 *
 * @author Carine Margrethe Rondeel
 * @version 1.0
 */
public class TrainDepartureRegister {
  private final Map<Integer, TrainDeparture> trainDepartureRegister;
  private LocalTime time;
  private static final String INVALID_TRAIN_NR_MESSAGE = "Invalid train number, it cannot be zero "
      + "or less than zero.";
  private static final String NO_DEPARTURE_MESSAGE = "No departures found with this train number.";

  /**
   * The constructor for the register, contains the "clock" that tracks what the time is. Time
   * initially is set to 00:00.
   */
  public TrainDepartureRegister() {
    trainDepartureRegister = new HashMap<>();
    this.time = LocalTime.MIN;
  }

  /**
   * Retrieves the train departure register.
   *
   * @return train departure register
   */
  public Map<Integer, TrainDeparture> getTrainDepartureRegister() {
    return trainDepartureRegister;
  }

  /**
   * Retrieves the current time.
   *
   * @return current time
   */
  public LocalTime getTime() {
    return time;
  }

  /**
   * Sets the time to be the newTime parameter, or throws an exception if it's earlier than or
   * equal to the current time.
   *
   * @param newTime the new time
   * @throws IllegalArgumentException if newTime is earlier or equal to current time
   */
  public void setTime(LocalTime newTime) throws IllegalArgumentException {
    if (newTime.isBefore(getTime()) || newTime.equals(getTime())) {
      throw new IllegalArgumentException("The time cannot be set to be earlier than or equal to "
          + "the current time, which is " + getTime() + ".");
    } else {
      this.time = newTime;
      removeDepartureAfterTimePassed();
    }
  }

  /**
   * Returns the departure info for given key, or returns null if there is no departure with said
   * number.
   *
   * @param trainNumber train number
   * @return departure info
   */
  public TrainDeparture getDepartureGivenTrainNr(int trainNumber) {
    if (trainDepartureRegister.containsKey(trainNumber)) {
      return trainDepartureRegister.get(trainNumber);
    }
    return null;
  }

  /**
   * Assigns a new track, given the train number. It first gets the info of the departure, which
   * will either return info or null. If the departure returns null, it will throw an exception.
   * If value is valid, it will set the new track.
   *
   * @param trainNr train number
   * @param newTrack new track number
   * @throws IllegalArgumentException if the train number is zero or less than zero
   * @throws NullPointerException if there are no departures found for the given train number
   */
  public void assignTrackGivenTrainNr(int trainNr, int newTrack) throws IllegalArgumentException,
      NullPointerException {
    if (trainNr <= 0) {
      throw new IllegalArgumentException(INVALID_TRAIN_NR_MESSAGE);
    }
    TrainDeparture td = getDepartureGivenTrainNr(trainNr);

    if (td == null) {
      throw new NullPointerException(NO_DEPARTURE_MESSAGE);
    }
    td.setTrack(newTrack);
  }

  /**
   * Assigns a new delay, given the train number. It first gets the info of the departure, which
   * will either return info or null. If the departure returns null, it will throw an exception and
   * stop running the method. If value is valid, it will set the new delay. In case delay is set
   * earlier than the current delay, it checks if departure time plus the new delay is before the
   * current time. If it is, an exception is thrown.
   *
   * @param trainNr train number
   * @param newDelay new delay
   * @throws IllegalArgumentException if the train number is zero or less than zero, or if new delay
   *      is attempted.to be set earlier than current time
   * @throws NullPointerException if the delay is null or if no departures are found
   */
  public void changeDelayGivenTrainNr(int trainNr, LocalTime newDelay)
      throws IllegalArgumentException, NullPointerException {
    if (trainNr <= 0) {
      throw new IllegalArgumentException(INVALID_TRAIN_NR_MESSAGE);
    }
    if (newDelay == null) {
      throw new NullPointerException("The delay cannot be null.");
    }

    TrainDeparture td = getDepartureGivenTrainNr(trainNr);
    if (td == null) {
      throw new NullPointerException(NO_DEPARTURE_MESSAGE);
    }

    int delayHour = newDelay.getHour();
    int delayMin = newDelay.getMinute();

    if (td.getDepartureTime().plusHours(delayHour).plusMinutes(delayMin).isBefore(time)) {
      throw new IllegalArgumentException("Delay cannot be set to earlier than the current time.");
    }

    td.setDelay(newDelay);
  }

  /**
   * Returns the amount of departures in the register.
   *
   * @return total amount of departures
   */
  public int getAmountRegisterDepartures() {
    return trainDepartureRegister.size();
  }

  /**
   * Add a train departure when track is given as a parameter. All values are checked if valid
   * in the constructor from TrainDeparture where they are implemented, and are therefore not
   * checked here.
   *
   * @param departureTime departure time
   * @param line line
   * @param trainNumber train number
   * @param destination destination
   * @param track track number
   * @throws IllegalArgumentException if a departure with this train number already exists, or if
   *        departure time is attempted to be set earlier than current time
   */
  public void addTrainDepartureToRegister(LocalTime departureTime, String line, int trainNumber,
                                          String destination, int track)
      throws IllegalArgumentException {
    if (trainDepartureRegister.containsKey(trainNumber)) {
      throw new IllegalArgumentException("Train number " + trainNumber + " already exists.");
    }
    if (departureTime.isBefore(getTime())) {
      throw new IllegalArgumentException("Departure time cannot be set earlier than the current "
          + "time, which is " + getTime() + ".");
    }

    trainDepartureRegister.put(trainNumber, new TrainDeparture(departureTime, line, trainNumber,
        destination, track));
  }

  /**
   * Add a train departure when track is not given as a parameter. All values are checked if valid
   * in the constructor from TrainDeparture where they are implemented, and are therefore not
   * checked here.
   *
   * @param departureTime departure time
   * @param line line
   * @param trainNumber train number
   * @param destination destination
   * @throws IllegalArgumentException if a departure with this train number already exists, or if
   *        departure time is attempted to be set earlier than current time
   */
  public void addTrainDepartureToRegister(LocalTime departureTime, String line, int trainNumber,
                                          String destination) throws IllegalArgumentException {
    if (trainDepartureRegister.containsKey(trainNumber)) {
      throw new IllegalArgumentException("Train number " + trainNumber + " already exists.");
    } else if (departureTime.isBefore(getTime())) {
      throw new IllegalArgumentException("Departure time cannot be set earlier than the current "
          + "time, which is " + getTime() + ".");
    }
    trainDepartureRegister.put(trainNumber, new TrainDeparture(departureTime, line, trainNumber,
        destination));
  }

  /**
   * Removes departure given train number. Tries to delete the instance with the given key, but
   * if it finds nothing, nothing gets removed, and an exception is thrown.
   *
   * @param trainNrToRemove train number that should be removed
   * @throws IllegalArgumentException if train number is less than zero or zero
   * @throws NoSuchElementException if nothing was removed if there were no departures registered
   *      for given train number
   */
  public void removeDepartureGivenTrainNr(int trainNrToRemove) throws IllegalArgumentException,
      NoSuchElementException {
    if (trainNrToRemove <= 0) {
      throw new IllegalArgumentException(INVALID_TRAIN_NR_MESSAGE);
    }
    boolean depWasRemoved = false;

    if (trainDepartureRegister.containsKey(trainNrToRemove)) {
      trainDepartureRegister.remove(trainNrToRemove);
      depWasRemoved = true;
    }

    if (!depWasRemoved) {
      throw new NoSuchElementException("There are no departures with this train number, so nothing "
          + "was removed.");
    }
  }

  /**
   * Removes departures based on if the departure time, in addition to potential delay, has passed.
   * This code was made with the help of ChatGPT. It makes an iterator for the register, and while
   * there still is something left to iterate over, it will. It takes the departures time and delay
   * and checks if those added together are before the current time. If it is, it will be removed.
   */
  public void removeDepartureAfterTimePassed() {
    Iterator<Map.Entry<Integer, TrainDeparture>> iterator =
        trainDepartureRegister.entrySet().iterator();

    while (iterator.hasNext()) {
      Map.Entry<Integer, TrainDeparture> entry = iterator.next();
      TrainDeparture trainDep = entry.getValue();
      LocalTime departureTime = trainDep.getDepartureTime();
      int delayHour = trainDep.getDelay().getHour();
      int delayMin = trainDep.getDelay().getMinute();

      if (departureTime.plusHours(delayHour).plusMinutes(delayMin).isBefore(time)) {
        iterator.remove();
      }
    }
  }

  /**
   * Returns a list of train departures going to the given destination by filtering out anything
   * going elsewhere. After that, sorts the list by departure time.
   * This code was improved by the help of ChatGPT. It returns an ArrayList where it turns the
   * values into a stream, and filters out anything not equal to the destination. It then sorts
   * based on the departure time, and makes it into a list.
   *
   * @param destination destination of the train
   * @return sorted list of train departures
   */
  public List<TrainDeparture> getDepartureGivenDestination(String destination) {
    String lowerDest = destination.toLowerCase();

    return new ArrayList<>(trainDepartureRegister.values().stream()
        .filter(trainDep -> trainDep.getDestination().toLowerCase().equals(lowerDest))
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .toList());
  }

  /**
   * Returns a list of train departures going to the given destination by filtering out any
   * departures that does not have a track, aka track is equal to -1.
   *
   * @return departure table
   */
  public List<TrainDeparture> getDepartureTable() {
    int invalidTrack = -1;

    return new ArrayList<>(trainDepartureRegister.values().stream()
        .filter(trainDep -> trainDep.getTrack() != invalidTrack)
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .toList());
  }

  /**
   * Returns string with the info for the given train number. If there is no departures associated
   * with the given train number, an empty string will be returned.
   *
   * @param trainNr train number
   * @return string with info on the departure, or empty string
   */
  public String getDepartureInformationStringTrainNr(int trainNr) {
    TrainDeparture td = getDepartureGivenTrainNr(trainNr);
    if (td != null) {
      return td.toString();
    }
    return "";
  }

  /**
   * Goes through a list with all the instances in the register, and for each it will add to the
   * string. If the list is empty it will return an empty string.
   *
   * @return string with everything in the register
   */
  @Override
  public String toString() {
    List<TrainDeparture> departureList = new ArrayList<>(trainDepartureRegister.values());
    if (departureList.isEmpty()) {
      return "";
    }

    departureList.sort(Comparator.comparing(TrainDeparture::getDepartureTime));

    StringBuilder departuresString = new StringBuilder();
    departureList.forEach((TrainDeparture departure) ->
        departuresString.append(departure.toString()).append("\n"));

    return departuresString.toString();
  }

}
