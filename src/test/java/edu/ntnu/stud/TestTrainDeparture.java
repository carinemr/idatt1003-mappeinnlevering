package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This is the class that contains the tests for the methods and constructors in the TrainDeparture
 * class.
 *
 * @author Carine Margrethe Rondeel
 * @version 1.0
 */
public class TestTrainDeparture {
  private TrainDeparture td;
  private final LocalTime DEPARTURE_TIME = LocalTime.of(13, 35);
  private final String LINE = "F2";
  private final int TRAIN_NUMBER = 31;
  private final String DESTINATION = "Oslo Lufthavn";
  private final int TRACK = 3;
  private final int NO_TRACK = -1;

  /**
   * Sets an initial test instance, that is used before each test.
   */
  @BeforeEach
  public void initTestIns() {
    td = new TrainDeparture(DEPARTURE_TIME,
        LINE, TRAIN_NUMBER, DESTINATION, TRACK);
  }

  @Test
  @DisplayName("TrainDeparture constructor with valid input with track, does not throw exception")
  public void testPosConstructorInclTrack() {
    assertEquals(DEPARTURE_TIME, td.getDepartureTime());
    assertEquals(LINE, td.getLine());
    assertEquals(TRAIN_NUMBER, td.getTrainNumber());
    assertEquals(DESTINATION, td.getDestination());
    assertEquals(TRACK, td.getTrack());
    assertEquals(LocalTime.MIN, td.getDelay());
  }

  @Test
  @DisplayName("TrainDeparture constructor with valid input without track, does not throw exception")
  public void testPosConstructorExclTrack() {
    TrainDeparture tdTest = new TrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER,
        DESTINATION);

    assertEquals(DEPARTURE_TIME, tdTest.getDepartureTime());
    assertEquals(LINE, tdTest.getLine());
    assertEquals(TRAIN_NUMBER, tdTest.getTrainNumber());
    assertEquals(DESTINATION, tdTest.getDestination());
    assertEquals(NO_TRACK, tdTest.getTrack());
    assertEquals(LocalTime.MIN, tdTest.getDelay());
  }

  @Test
  @DisplayName("TrainDeparture constructor with invalid departure time, throws IllegalArgumentException")
  public void testNegConstructorDepTime() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(null, LINE, TRAIN_NUMBER,
        DESTINATION, TRACK));
  }

  @Test
  @DisplayName("TrainDeparture constructor with invalid line, throws IllegalArgumentException")
  public void testNegConstructorLine() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, null, TRAIN_NUMBER,
        DESTINATION, TRACK));
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, "", TRAIN_NUMBER,
        DESTINATION, TRACK));
  }

  @Test
  @DisplayName("TrainDeparture constructor with invalid train number, throws IllegalArgumentException")
  public void testNegConstructorTrainNumber() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, LINE, 0,
        DESTINATION, TRACK));
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, LINE, -2,
        DESTINATION, TRACK));
  }

  @Test
  @DisplayName("TrainDeparture constructor with invalid destination, throws IllegalArgumentException")
  public void testNegConstructorDestination() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER,
        null, TRACK));
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER,
        "", TRACK));
  }

  @Test
  @DisplayName("TrainDeparture constructor with invalid track, throws IllegalArgumentException")
  public void testNegConstructorTrack() {
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER,
        DESTINATION, 0));
    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(DEPARTURE_TIME, LINE, TRAIN_NUMBER,
        DESTINATION, -2));
  }

  @Test
  @DisplayName("SetDelay with valid delay, does not throw exception")
  public void testSetPosDelay() {
    LocalTime newDelay = LocalTime.of(0, 9);

    td.setDelay(newDelay);

    assertEquals(newDelay, td.getDelay());
  }

  @Test
  @DisplayName("SetDelay with invalid delay, throws IllegalArgumentException")
  public void testSetNegDelay() {
    assertThrows(IllegalArgumentException.class, () -> td.setDelay(null));
  }

  @Test
  @DisplayName("SetTrack with valid tracks, does not throw exceptions")
  public void testSetPosTrack() {
    int newTrack1 = 1;
    int newTrack2 = -1;

    td.setTrack(newTrack1);
    assertEquals(newTrack1, td.getTrack());

    td.setTrack(newTrack2);
    assertEquals(newTrack2, td.getTrack());
  }

  @Test
  @DisplayName("SetTrack with invalid tracks, throws IllegalArgumentException")
  public void testSetNegTrack() {
    int newTrack1 = -2;
    int newTrack2 = 0;

    assertThrows(IllegalArgumentException.class, () -> td.setTrack(newTrack1));
    assertThrows(IllegalArgumentException.class, () -> td.setTrack(newTrack2));
  }
}
