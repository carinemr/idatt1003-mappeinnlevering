package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This is the class that contains the tests for the methods and constructors in the
 * TrainDepartureRegister class. For positive testing I'll use values I know are valid and check
 * that methods work correctly. For negative testing I'll check if exceptions are thrown properly
 * where they should, and that all methods are properly secured.
 *
 * @author Carine Margrethe Rondeel
 * @version 1.0
 */
public class TestTrainDepartureRegister {
  private TrainDepartureRegister tdr;
  private final LocalTime INITIAL_TIME = LocalTime.MIN;
  private final LocalTime DEPARTURE_TIME_1 = LocalTime.of(13, 35);
  private final LocalTime DEPARTURE_TIME_2 = LocalTime.of(13, 29);
  private final LocalTime DEPARTURE_TIME_3 = LocalTime.of(13, 32);
  private final LocalTime DEPARTURE_TIME_4 = LocalTime.of(13, 40);
  private final String LINE_1 = "F2";
  private final String LINE_2 = "L12";
  private final String LINE_3 = "L12";
  private final String LINE_4 = "L1";
  private final int TRAIN_NUMBER_1 = 31;
  private final int TRAIN_NUMBER_2 = 62;
  private final int TRAIN_NUMBER_3 = 63;
  private final int TRAIN_NUMBER_4 = 10;
  private final int INVALID_TRAIN_NUMBER = 2;
  private final String DESTINATION_1 = "Eidsvoll";
  private final String DESTINATION_2 = "Eidsvoll";
  private final String DESTINATION_3 = "Kongsberg";
  private final String DESTINATION_4 = "Lillestrøm";
  private final int TRACK_1 = 3;
  private final int TRACK_2 = 1;
  private final int TRACK_4 = 4;
  private final int NO_TRACK = -1;
  private final int EXPECTED_DEP_AMOUNT = 3;
  private final LocalTime VALID_DELAY = LocalTime.of(0, 14);

  /**
   * Sets up an initial register, current time automatically gets set to 00:00.
   */
  @BeforeEach
  public void initTestReg() {
    tdr = new TrainDepartureRegister();
    tdr.addTrainDepartureToRegister(DEPARTURE_TIME_1, LINE_1, TRAIN_NUMBER_1, DESTINATION_1, TRACK_1);
    tdr.addTrainDepartureToRegister(DEPARTURE_TIME_2, LINE_2, TRAIN_NUMBER_2, DESTINATION_2, TRACK_2);
    tdr.addTrainDepartureToRegister(DEPARTURE_TIME_3, LINE_3, TRAIN_NUMBER_3, DESTINATION_3);
  }

  @Test
  @DisplayName("Tests that the constructor gets set up correctly and the instances are added")
  public void testConstructor() {
    assertEquals(INITIAL_TIME, tdr.getTime());
    assertNotNull(tdr.getTrainDepartureRegister());
    assertEquals(EXPECTED_DEP_AMOUNT, tdr.getAmountRegisterDepartures());
  }

  @Test
  @DisplayName("SetTime with valid time-input, does not throw exception")
  public void testSetPosTime() {
    LocalTime newTime = LocalTime.of(12, 40);

    tdr.setTime(newTime);

    assertEquals(newTime, tdr.getTime());
  }

  @Test
  @DisplayName("SetTime with invalid time-input, before and equal to current time, throws IllegalArgumentException")
  public void testSetNegTime() {
    tdr.setTime(LocalTime.of(12, 40));
    LocalTime invalidTimeBefore = LocalTime.of(10, 23);
    LocalTime invalidTimeEqual = LocalTime.of(12, 40);

    assertThrows(IllegalArgumentException.class, () -> tdr.setTime(invalidTimeBefore));
    assertThrows(IllegalArgumentException.class, () -> tdr.setTime(invalidTimeEqual));
  }

  @Test
  @DisplayName("GetDepartmentGivenTrainNr with valid train number, does not return null")
  public void testPosGetDepGivenTrainNr() {
    assertNotNull(tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_2));
  }
  @Test
  @DisplayName("GetDepartmentGivenTrainNr with invalid train number, returns null")
  public void testNegGetDepGivenTrainNr() {
    assertNull(tdr.getDepartureGivenTrainNr(INVALID_TRAIN_NUMBER));
  }

  @Test
  @DisplayName("AssignTrack with valid values, does not throw exceptions")
  public void testAssignPosTrack() {
    int newTrack1 = 2;
    int newTrack2 = 3;

    tdr.assignTrackGivenTrainNr(TRAIN_NUMBER_1, newTrack1);
    assertEquals(newTrack1, tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_1).getTrack());

    tdr.assignTrackGivenTrainNr(TRAIN_NUMBER_3, newTrack2);
    assertEquals(newTrack2, tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_3).getTrack());
  }

  @Test
  @DisplayName("AssignTrack with invalid values, throws Ill.Arg.Exc or Null.Ptr.Exc")
  public void testAssignNegTrack() {
    int validTrack = 2;
    int invalidTrack = -3;

    assertThrows(NullPointerException.class, () ->
        tdr.assignTrackGivenTrainNr(INVALID_TRAIN_NUMBER, validTrack));
    assertThrows(IllegalArgumentException.class, () ->
        tdr.assignTrackGivenTrainNr(TRAIN_NUMBER_1, invalidTrack));
  }

  @Test
  @DisplayName("ChangeDelay test with valid new delay, does not throw exception")
  public void testChangePosDelay() {
    tdr.changeDelayGivenTrainNr(TRAIN_NUMBER_1, VALID_DELAY);
    assertEquals(VALID_DELAY, tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_1).getDelay());
  }

  @Test
  @DisplayName("ChangeDelay test for invalid new delay and invalid train number, throws NullPointerException")
  public void testChangeNegDelay() {
    LocalTime invalidDelay = null;

    assertThrows(NullPointerException.class, () ->
        tdr.changeDelayGivenTrainNr(INVALID_TRAIN_NUMBER, VALID_DELAY));
    assertThrows(NullPointerException.class, () ->
        tdr.changeDelayGivenTrainNr(TRAIN_NUMBER_1, invalidDelay));
  }

  @Test
  @DisplayName("ChangeDelay test if attempted new delay makes delay plus departure before current time, throws Ill.Arg.Exc")
  public void testChangeNegDelayBeforeCurrentTime() {
    LocalTime delayThatMakesDepBeforeCurrentTime = LocalTime.of(0, 5);

    tdr.changeDelayGivenTrainNr(TRAIN_NUMBER_2, VALID_DELAY);
    tdr.setTime(LocalTime.of(13, 35));

    assertThrows(IllegalArgumentException.class, () ->
        tdr.changeDelayGivenTrainNr(TRAIN_NUMBER_2, delayThatMakesDepBeforeCurrentTime));
  }

  @Test
  @DisplayName("Test that GetAmountDepartures returns expected amount")
  public void testGetAmountDepartures() {
    assertEquals(EXPECTED_DEP_AMOUNT, tdr.getAmountRegisterDepartures());
  }

  @Test
  @DisplayName("AddTrainDeparture including track with all valid values, does not throw exceptions")
  public void testPosAddTrainDepartureInclTrack() {
    tdr.addTrainDepartureToRegister(DEPARTURE_TIME_4, LINE_4, TRAIN_NUMBER_4, DESTINATION_4, TRACK_4);

    TrainDeparture td = tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_4);

    assertEquals(DEPARTURE_TIME_4, td.getDepartureTime());
    assertEquals(LINE_4, td.getLine());
    assertEquals(TRAIN_NUMBER_4, td.getTrainNumber());
    assertEquals(DESTINATION_4, td.getDestination());
    assertEquals(TRACK_4, td.getTrack());
    assertEquals(INITIAL_TIME, td.getDelay());
  }

  @Test
  @DisplayName("Tests addTrainDeparture including track for previously occupied train number and for departure time set before current time, throws Ill.Arg.Exc")
  public void testNegAddTrainDepartureInclTrack() {
    int previouslyOccupiedTrainNumber = TRAIN_NUMBER_1;
    LocalTime invalidDepartureTime = LocalTime.of(10, 51);

    tdr.setTime(LocalTime.of(12, 40));

    assertThrows(IllegalArgumentException.class, () ->
        tdr.addTrainDepartureToRegister(DEPARTURE_TIME_4, LINE_4, previouslyOccupiedTrainNumber,
            DESTINATION_4, TRACK_4));
    assertThrows(IllegalArgumentException.class, () ->
        tdr.addTrainDepartureToRegister(invalidDepartureTime, LINE_4, TRAIN_NUMBER_4,
            DESTINATION_4, TRACK_4));
  }

  @Test
  @DisplayName("AddTrainDeparture excluding track with all valid values, does not throw exceptions")
  public void testPosAddTrainDepartureExclTrack() {
    tdr.addTrainDepartureToRegister(DEPARTURE_TIME_4, LINE_4, TRAIN_NUMBER_4, DESTINATION_4);

    TrainDeparture td = tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_4);

    assertEquals(DEPARTURE_TIME_4, td.getDepartureTime());
    assertEquals(LINE_4, td.getLine());
    assertEquals(TRAIN_NUMBER_4, td.getTrainNumber());
    assertEquals(DESTINATION_4, td.getDestination());
    assertEquals(NO_TRACK, td.getTrack());
    assertEquals(INITIAL_TIME, td.getDelay());
  }

  @Test
  @DisplayName("Tests addTrainDeparture excluding track for previously occupied train number and for departure time set before current time, throws Ill.Arg.Exc")
  public void testNegAddTrainDepartureExclTrack() {
    int previouslyOccupiedTrainNumber = TRAIN_NUMBER_2;
    LocalTime invalidDepartureTime = LocalTime.of(11, 10);

    tdr.setTime(LocalTime.of(12, 20));

    assertThrows(IllegalArgumentException.class, () ->
        tdr.addTrainDepartureToRegister(DEPARTURE_TIME_4, LINE_4, previouslyOccupiedTrainNumber,
            DESTINATION_4));
    assertThrows(IllegalArgumentException.class, () ->
        tdr.addTrainDepartureToRegister(invalidDepartureTime, LINE_4, TRAIN_NUMBER_4,
            DESTINATION_4));
  }

  @Test
  @DisplayName("RemoveDepartureGivenTrainNumber removed departure, after removal expected size equal to actual, does not throw exceptions")
  public void testPosRemoveDepGivenTrainNr() {
    int registerSizeBeforeRemoval = tdr.getAmountRegisterDepartures();
    int expectedSizeAfterRemoval = registerSizeBeforeRemoval - 1;

    tdr.removeDepartureGivenTrainNr(TRAIN_NUMBER_2);

    assertEquals(expectedSizeAfterRemoval, tdr.getAmountRegisterDepartures());
  }

  @Test
  @DisplayName("RemoveDepartureGivenTrainNumber removed departure, after failed removal previous size equal to actual, throws exceptions")
  public void testNegRemoveDepGivenTrainNr() {
    int registerSizeBeforeRemoval = tdr.getAmountRegisterDepartures();
    int negTrainNr = -2;

    //Test 1 & 2
    assertThrows(NoSuchElementException.class, () ->
        tdr.removeDepartureGivenTrainNr(INVALID_TRAIN_NUMBER));
    assertThrows(IllegalArgumentException.class, () ->
        tdr.removeDepartureGivenTrainNr(negTrainNr));

    //Test 3
    try {
      tdr.removeDepartureGivenTrainNr(INVALID_TRAIN_NUMBER);
    } catch (NoSuchElementException e) {
      /* Could have had a print message here, but decided to leave it empty, as seeing that the
      register size remains the same is enough.*/
    }
    int registerSizeAfterAttempt = tdr.getAmountRegisterDepartures();
    assertEquals(registerSizeBeforeRemoval, registerSizeAfterAttempt);
  }

  @Test
  @DisplayName("Remove departure after current time is passed, after time passed departures are removed, matches expected size")
  public void testPosRemoveDepAfterTimePassed() {
    int expectedSizeAfterRemoval = 1;
    LocalTime timeAfterTwoDep = LocalTime.of(13, 33);

    tdr.setTime(timeAfterTwoDep);
    tdr.removeDepartureAfterTimePassed();

    assertEquals(expectedSizeAfterRemoval, tdr.getAmountRegisterDepartures());
  }

  @Test
  @DisplayName("Remove departure after current time is passed, time to early for any to leave, so none removed")
  public void testNegRemoveDepAfterTimePassed() {
    int registerSizeBeforeRemoval = tdr.getAmountRegisterDepartures();
    LocalTime timeBeforeAnyLeaves = LocalTime.of(13, 25);

    tdr.setTime(timeBeforeAnyLeaves);
    tdr.removeDepartureAfterTimePassed();

    assertEquals(registerSizeBeforeRemoval, tdr.getAmountRegisterDepartures());
  }

  @Test
  @DisplayName("Test getting departures given destination, with different letter-cases, destination that is not there, and empty string, does not throw")
  public void testGetDeparturesGivenDestination() {
    List<TrainDeparture> validList1 = tdr.getDepartureGivenDestination(DESTINATION_1);
    List<TrainDeparture> validList2 = tdr.getDepartureGivenDestination("eIDsvoLL");

    int expectedValidSize = 2;
    List<TrainDeparture> emptyList1 = tdr.getDepartureGivenDestination("Example");
    List<TrainDeparture> emptyList2 = tdr.getDepartureGivenDestination("");

    assertNotEquals(emptyList2, validList1);
    assertEquals(validList1, validList2);
    assertEquals(expectedValidSize, validList1.size());
    assertNotNull(emptyList1);
    assertNotNull(emptyList2);
  }

  @Test
  @DisplayName("Test that departure table is returned, not null, equal to expected sizes 2 and 0 for empty")
  public void testGetDepartureTable() {
    List<TrainDeparture> validList = tdr.getDepartureTable();
    int expectedValidSize = 2;
    TrainDepartureRegister tdrEmpty = new TrainDepartureRegister();
    List<TrainDeparture> emptyList = tdrEmpty.getDepartureTable();
    int expectedEmptySize = 0;

    assertEquals(expectedValidSize, validList.size());
    assertEquals(expectedEmptySize, emptyList.size());
    assertNotNull(emptyList);
  }

  @Test
  @DisplayName("Tests get departure info from train number, that info is not null, and equals empty string or equals expected output")
  public void testPosDepartureInformationTrainNr() {
    String information = tdr.getDepartureInformationStringTrainNr(TRAIN_NUMBER_2);

    assertNotNull(information);
    assertNotEquals("", information);
    assertEquals(tdr.getDepartureGivenTrainNr(TRAIN_NUMBER_2).toString(), information);
  }

  @Test
  @DisplayName("Tests get departure info from train number, given invalid train number, checks that output is empty string")
  public void testNegDepartureInformationTrainNr() {
    String invalidInformation = tdr.getDepartureInformationStringTrainNr(INVALID_TRAIN_NUMBER);

    assertNotNull(invalidInformation);
    assertEquals("", invalidInformation);
  }
}