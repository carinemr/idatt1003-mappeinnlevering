# Portfolio project IDATT1003 - 2023

STUDENT NAME = Carine Margrethe Rondeel  
STUDENT ID = 10024

## Project description

[//]: # (TODO: Write a short description of your project/product here.)
The premise of this project was to create a simplified system for maintaining a train dispatch system. This includes ways to display departures, search for them, add/remove and change certain parts of said departures, including a built in clock that the user can change and this will change which departures are displayed.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
All sourcefiles stored in the main folder, then the java folder and then within the edu.ntnu.stud package, while the test classes are put into the test folder, which is on the same level as main, with the same setup within.

## Link to repository

[//]: # (TODO: Include a link to your repository here.)
https://gitlab.stud.idi.ntnu.no/carinemr/idatt1003-mappeinnlevering/-/tree/main
## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
To run this project, you have to run the TrainDispatchApp.java file, where the main method is 'main' itself. Once this is ran, it will call upon init() to initalize and start() to start the program, which are in the UserInterface file. But as a user, you only have to interact with TrainDispatchApp.java!

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
To run the tests, you just have to open either TestTrainDeparture to test the individual train departure and the methods used for it, or you can open TestTrainDepartureRegister to test all the methods relating to the register, and testing that adding/removing and changing stuff within the register passes the test. 

## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
ChatGPT (https://chat.openai.com)